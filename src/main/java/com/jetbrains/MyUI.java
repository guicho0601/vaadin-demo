package com.jetbrains;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    private int indexOfEstudiante = -1;
    private final String MEJOR_ESTUDIANTE = "El mejore estudiante es: ";


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final Profesor profesor = new Profesor("Luis Ramos", "Programación");
        final VerticalLayout verticalLayout = new VerticalLayout();
        final HorizontalLayout horizontalLayout = new HorizontalLayout();
        final HorizontalLayout gridsHorizontalLayout = new HorizontalLayout();
        final VerticalLayout verticalLayoutAsignaturas = new VerticalLayout();

        final Label titulo = new Label("Asignaturas");
        titulo.setStyleName("h1");
        final VerticalLayout vLProfesor = new VerticalLayout(
                new HorizontalLayout(titulo, new VerticalLayout(
                        new Label("Nombre: " + profesor.getNombre()),
                        new Label("Especialidad: " + profesor.getEspecialidad())
                ))
        );

        final TextField name = new TextField("Nombre:");
        final TextField age = new TextField("Edad:");
        final Label mejorEstudiante = new Label();

        Estudiante estudiante = new Estudiante("Luis", 26);
        estudiante.agregarAsignatura(new Asignatura("Matemática", 71, 3));
        profesor.getEstudiantes().add(estudiante);

        Grid<Estudiante> grid = new Grid<>();
        grid.setWidth("100%");
        grid.setItems(profesor.getEstudiantes());
        grid.addColumn(Estudiante::getNombre).setCaption("Nombre");
        grid.addColumn(Estudiante::getEdad).setCaption("Edad");
        grid.addColumn(Estudiante::getPromedio).setCaption("Promedio");

        Grid<Asignatura> asignaturaGrid = new Grid<>();
        asignaturaGrid.addColumn(Asignatura::getNombre).setCaption("Nombre asignatura");
        asignaturaGrid.addColumn(Asignatura::getCantEvaluaciones).setCaption("Cantidad de evaluaciones");
        asignaturaGrid.addColumn(Asignatura::getNota).setCaption("Nota");

        Button button = new Button("Agregar");
        button.addClickListener(e -> {
            profesor.getEstudiantes().add(new Estudiante(name.getValue(), Integer.parseInt(age.getValue())));
            grid.setItems(profesor.getEstudiantes());
            name.clear();
            age.clear();
            name.focus();
            mejorEstudiante.setValue(MEJOR_ESTUDIANTE + profesor.mejorEstudiante().getNombre());
            Notification.show("Estudiante agregado");
        });

        grid.addItemClickListener(itemClick -> {
            Estudiante est = itemClick.getItem();
            asignaturaGrid.setItems(est.getAsignaturas());
            indexOfEstudiante = profesor.getEstudiantes().indexOf(est);
            gridsHorizontalLayout.addComponents(verticalLayoutAsignaturas);
        });

        final TextField nombreAsignatura = new TextField("Nombre asignatura:");
        final TextField notaAsignatura = new TextField("Nota:");
        final TextField cantidadEvaluaciones = new TextField("Cantidad de evaluaciones");
        Button addAsignatura = new Button("Agregar");
        addAsignatura.addClickListener(clickEvent -> {
            Asignatura asignatura = new Asignatura(nombreAsignatura.getValue(), Integer.parseInt(notaAsignatura.getValue()), Integer.parseInt(cantidadEvaluaciones.getValue()));
            profesor.getEstudiantes().get(indexOfEstudiante).agregarAsignatura(asignatura);
            asignaturaGrid.setItems(profesor.getEstudiantes().get(indexOfEstudiante).getAsignaturas());
            nombreAsignatura.clear();
            notaAsignatura.clear();
            cantidadEvaluaciones.clear();
            mejorEstudiante.setValue(MEJOR_ESTUDIANTE + profesor.mejorEstudiante().getNombre());
            grid.setItems(profesor.getEstudiantes());
            Notification.show("Asignatura agregada");
        });
        Button cerrarAsignaturas = new Button("Cerrar");
        cerrarAsignaturas.addClickListener(clickEvent -> {
            gridsHorizontalLayout.removeComponent(verticalLayoutAsignaturas);
            grid.setItems(profesor.getEstudiantes());
            mejorEstudiante.setValue(MEJOR_ESTUDIANTE + profesor.mejorEstudiante().getNombre());
        });
        HorizontalLayout asignaturaHorizontalLayout = new HorizontalLayout(
                nombreAsignatura,
                notaAsignatura,
                cantidadEvaluaciones,
                addAsignatura,
                cerrarAsignaturas
        );
        asignaturaHorizontalLayout.setComponentAlignment(addAsignatura, Alignment.BOTTOM_CENTER);
        asignaturaHorizontalLayout.setComponentAlignment(cerrarAsignaturas, Alignment.BOTTOM_CENTER);
        verticalLayoutAsignaturas.addComponents(asignaturaHorizontalLayout, asignaturaGrid);

        horizontalLayout.addComponents(name, age, button);
        horizontalLayout.setComponentAlignment(button, Alignment.BOTTOM_CENTER);
        gridsHorizontalLayout.addComponents(new VerticalLayout(horizontalLayout, grid, mejorEstudiante));
        mejorEstudiante.setValue(MEJOR_ESTUDIANTE + profesor.mejorEstudiante().getNombre());

        verticalLayout.addComponents(
                vLProfesor,
                gridsHorizontalLayout
        );

        setContent(verticalLayout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
